<?php

declare(strict_types=1);

namespace HakimCh\ReCaptchaBundle\Controller;

use HakimCh\ReCaptchaBundle\Exceptions\ReCaptchaBadRequestException;
use ReCaptcha\ReCaptcha;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/google/api/recaptcha/verify", name="google_api_recaptcha_verify")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function verify(Request $request): Response
    {
        $response = json_decode($request->getContent(), true);
        if (!isset($response['action'], $response['token'])) {
            throw new ReCaptchaBadRequestException('Missing parameter in the request');
        }
        $recaptcha = new ReCaptcha($this->getParameter('google_recaptcha_secret'));
        $response = $recaptcha->setExpectedHostname($request->server->get('SERVER_NAME'))
            ->setExpectedAction($response['action'])
            ->setScoreThreshold(1)
            ->verify(
                $response['token'],
                $request->server->get('REMOTE_ADDR')
            );

        return $this->json($response->toArray());
    }
}

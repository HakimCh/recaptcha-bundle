<?php

declare(strict_types=1);

namespace HakimCh\ReCaptchaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReCaptchaType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped' => false,
            'translation_domain' => 'form',
            'attr' => ['value' => 'send'],
        ]);
    }

    public function getBlockPrefix()
    {
        return 're_captcha_submit';
    }

    public function getParent()
    {
        return TextType::class;
    }
}

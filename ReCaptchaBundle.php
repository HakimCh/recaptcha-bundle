<?php

declare(strict_types=1);

namespace HakimCh\ReCaptchaBundle;

use HakimCh\ReCaptchaBundle\DependencyInjection\Compiler\ReCaptchaPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ReCaptchaBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ReCaptchaPass());
    }
}

<?php

declare(strict_types=1);

namespace HakimCh\ReCaptchaBundle\Exceptions;

class ReCaptchaBadRequestException extends \Exception
{
}

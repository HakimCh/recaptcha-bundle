<?php

declare(strict_types=1);

namespace HakimCh\ReCaptchaBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ReCaptchaPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');
        $resources[] = '@ReCaptcha/fields.html.twig';
        $container->setParameter('twig.form.resources', $resources);

        $twigDefinition = $container->getDefinition('twig');
        $twigDefinition->addMethodCall(
            'addGlobal',
            ['google_recaptcha_site_key', $container->getParameter('google_recaptcha_site_key')]
        );
    }
}
